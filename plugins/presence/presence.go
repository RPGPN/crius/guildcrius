package presence

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius/guildcrius/permissions"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"time"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Presence",
	Creator:            "Ben <Ponkey364>",
	License:            "MPL-2.0",
	URL:                "https://gitlab.com/RPGPN/guildcrius/-/tree/hosted/plugins/presence",
	Description:        "Bot Presence Commands" + permissions.CMD_BOT_OWNER_ONLY,
	SupportedPlatforms: cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "Set Presence",
			HandlerName:        "setpres",
			Activator:          "setpres",
			SupportedPlatforms: cc.PlatformDiscord,
			Help:               "Set the bot's presence",
		},
		{
			Name:               "Presence Cycle",
			HandlerName:        "prescycle",
			Activator:          "prescycle",
			Help:               "Presence Cycle",
			SupportedPlatforms: cc.PlatformDiscord,
			Subcommands: []*cc.PJCommand{
				{
					Name:               "Start",
					HandlerName:        "start",
					Activator:          "start",
					Help:               "Start the presence cycle. Usage: `prescycle start [time (e.g. 15m / 1h)]`",
					SupportedPlatforms: cc.PlatformDiscord,
				},
				{
					Name:               "Stop",
					HandlerName:        "stop",
					Activator:          "stop",
					Help:               "Stop the presence cycle",
					SupportedPlatforms: cc.PlatformDiscord,
				},
				{
					Name:               "Add To List",
					HandlerName:        "additem",
					Activator:          "additem",
					Help:               "Add an item to the potential presences. Usage: `prescycle additem [status (playing/listening/watching)] [text]`",
					SupportedPlatforms: cc.PlatformDiscord,
				},
				{
					Name:               "Remove From List",
					HandlerName:        "rmitem",
					Activator:          "rmitem",
					Help:               "Remove an item from the potential presences. Usage: `prescycle rmitem [id]`",
					SupportedPlatforms: cc.PlatformDiscord,
				},
				{
					Name:               "Set Tick Period",
					HandlerName:        "setperiod",
					Activator:          "setperiod",
					Help:               "Set how often to change the presence. Usage: `prescycle setperiod [time (e.g. 15m / 1h)]`",
					SupportedPlatforms: cc.PlatformDiscord,
				},
				{
					Name:               "List Items",
					HandlerName:        "listitems",
					Activator:          "listitems",
					Help:               "List the potential presences",
					SupportedPlatforms: cc.PlatformDiscord,
				},
			},
		},
	},
}

type presence struct {
	perms       GuildCrius.Permissions
	settings    CriusUtils.Settings
	session     *discordgo.Session
	ticker      *time.Ticker
	tickerStop  chan bool
	tickerReset *time.Duration
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	perms := GuildCrius.GetPermissions(ctx)
	settings := CriusUtils.GetSettings(ctx)
	session := ctx.Value(cc.PlatformDiscord.ToString()).(*discordgo.Session)

	p := &presence{
		perms:    perms,
		settings: settings,
		session:  session,
	}

	return map[string]cc.CommandHandler{
		"setpres":             p.SetPresence,
		"prescycle/start":     p.CycleStart,
		"prescycle/stop":      p.CycleStop,
		"prescycle/additem":   p.CycleAddItem,
		"prescycle/rmitem":    p.CycleRemoveItem,
		"prescycle/setperiod": p.CycleSetPeriod,
		"prescycle/listitems": p.CycleListItems,
	}, nil
}

func (p *presence) SetPresence(m *cc.MessageContext, args []string) error {
	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	if len(args) == 0 {
		err := p.session.UpdateStatus(0, "")
		if err != nil {
			return err
		}
		return nil
	}

	var err error
	switch args[0] {
	case "listening":
		err = p.session.UpdateListeningStatus(args[1])
	case "playing":
		err = p.session.UpdateStatus(0, args[1])
	case "watching":
		err = p.session.UpdateStatusComplex(discordgo.UpdateStatusData{
			IdleSince: nil,
			Game: &discordgo.Game{
				Name: args[1],
				Type: discordgo.GameTypeWatching,
			},
			AFK:    false,
			Status: "online",
		})
	}

	return err
}

type presItem struct {
	ID          int
	Presence    string `db:"pres"`
	StatusType  string `db:"status_type"`
	WasLastUsed bool   `db:"was_last_used"`
}

func (p *presence) CycleStart(m *cc.MessageContext, args []string) error {
	if len(args) != 1 {
		m.Send("Usage: `prescycle start [time (e.g. 15m / 1h)]`")
	}

	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	dur, err := time.ParseDuration(args[0])
	if err != nil {
		return err
	}

	if dur < time.Minute {
		m.Send("Please set a period longer than 1 minute")
		return nil
	}

	p.ticker = time.NewTicker(dur)

	go p.tickerFunc()

	m.Send(fmt.Sprintf("Started a ticker with period %s", args[0]))

	return nil
}

func (p *presence) CycleStop(m *cc.MessageContext, _ []string) error {
	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	return nil
}

func (p *presence) CycleAddItem(m *cc.MessageContext, args []string) error {
	if len(args) < 2 {
		m.Send("Usage: `prescycle additem [status (playing/listening/watching)] [text]`")
	}

	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	switch args[0] {
	case "playing", "watching", "listening":
		break
	default:
		m.Send("Usage: `prescycle additem [status (playing/listening/watching)] [text]`")
	}

	_, err := p.settings.GetDB().Exec("INSERT INTO pres_cycle (pres, status_type) VALUES ($1, $2)", args[1], args[0])
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Added %s %s to the presence list", args[0], args[1]))

	return nil
}

func (p *presence) CycleRemoveItem(m *cc.MessageContext, args []string) error {
	if len(args) != 1 {
		m.Send("Usage: `prescycle rmitem [id]`")
	}

	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	_, err := p.settings.GetDB().Exec("DELETE FROM pres_cycle WHERE id=$1", args[0])
	if err != nil {
		return err
	}

	m.Send("Removed presence item id " + args[0])

	return nil
}

func (p *presence) CycleSetPeriod(m *cc.MessageContext, args []string) error {
	if len(args) != 1 {
		m.Send("Usage: `prescycle setperiod [time (e.g. 15m / 1h)]`")
	}

	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	dur, err := time.ParseDuration(args[0])
	if err != nil {
		return err
	}

	if dur < time.Minute {
		m.Send("Please set a period longer than 1 minute")
		return nil
	}

	p.tickerReset = &dur

	return nil
}

func (p *presence) CycleListItems(m *cc.MessageContext, _ []string) error {
	if !p.perms.IsBotOwner(m.Author.ID) {
		m.Send(permissions.ErrNotOwner)
		return nil
	}

	items := []*struct {
		Presence   string `db:"pres"`
		StatusType string `db:"status_type"`
	}{}

	err := p.settings.GetDB().Select(&items, "SELECT pres, status_type FROM pres_cycle")
	if err != nil {
		return err
	}

	msg := "Presence Cycle Items:\n"
	for i, v := range items {
		msg += fmt.Sprintf("%d. %s %s\n", i+1, v.StatusType, v.Presence)
	}

	m.Send(msg)

	return nil
}

func (p *presence) tickerFunc() {
	db := p.settings.GetDB()

	for {
		select {
		case <-p.tickerStop:
			return
		case <-p.ticker.C:
			{
				item := &presItem{}

				err := db.Get(item, "SELECT * FROM pres_cycle WHERE was_last_used=false ORDER BY random() LIMIT 1;")
				if err != nil {
					logrus.Error(err)
					continue
				}

				// set was_last_used on all of em to false
				_, err = db.Exec("UPDATE pres_cycle SET was_last_used=false")
				if err != nil {
					// we don't care about the error too much, yeah it's annoying, but who cares if we show the same joke twice?
					logrus.Error(err)
				}

				// now mark the current one.
				_, err = db.Exec("UPDATE pres_cycle SET was_last_used=true WHERE id=$1", item.ID)
				if err != nil {
					// see above
					logrus.Error(err)
				}

				// and throw it to Discord
				switch item.StatusType {
				case "listening":
					err = p.session.UpdateListeningStatus(item.Presence)
				case "playing":
					err = p.session.UpdateStatus(0, item.Presence)
				case "watching":
					err = p.session.UpdateStatusComplex(discordgo.UpdateStatusData{
						IdleSince: nil,
						Game: &discordgo.Game{
							Name: item.Presence,
							Type: discordgo.GameTypeWatching,
						},
						AFK:    false,
						Status: "online",
					})
				default:
					logrus.Warnf("Item %d has a bad StatusType %s", item.ID, item.StatusType)
				}
				if err != nil {
					logrus.Error(err)
					return
				}

				if p.tickerReset != nil {
					logrus.Tracef("Setting presence ticker duration to %s", p.tickerReset.String())
					p.ticker.Reset(*p.tickerReset)

					p.tickerReset = nil
				}
			}
		}
	}
}
