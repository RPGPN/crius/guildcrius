package firehose

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius/guildcrius/permissions"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/ponkey364/glimwsc"
	"net/http"
	"os"
	"time"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Firehose",
	License:            "MPL-2.0",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/guildcrius/-/tree/hosted/firehose",
	Description:        "Firehose notifications plugin",
	SupportedPlatforms: cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "Add Firehose Watch",
			Activator:          "addfhwatch",
			Help:               "Adds a channel to watch to the list. Usage: addfhwatch [service] [username (or channel URL for youtube)] [message] #channel",
			HandlerName:        "addfhwatch",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Remove Firehose Watch",
			Activator:          "remfhwatch",
			Help:               "Removes a watch from a channel. Usage: remfhwatch [service] [username (or channel URL for youtube)] #channel",
			HandlerName:        "remfhwatch",
			SupportedPlatforms: cc.PlatformDiscord,
		},
	},
}

// a standardised struct for use in templates
type fhGoLive struct {
	Username string
	Service  firehoseService
	Category string
	Title    string
	Link     string
}

type firehose struct {
	db                 *sqlx.DB
	permissions        GuildCrius.Permissions
	settings           *settings.Settings
	glimwsc            *glimwsc.Session
	ctx                context.Context
	ytTicker           *time.Ticker
	ytDone             chan bool
	pWatchCount        *prometheus.GaugeVec
	pNotificationsSeen *prometheus.GaugeVec
	pNotificationsSent *prometheus.GaugeVec
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	s := ctx.Value("settings").(*settings.Settings)
	//bus := CriusUtils.GetBus(ctx) // TODO: we'll sub to cc.EventCommanderClose to stop any stuff we're running
	permissions := GuildCrius.GetPermissions(ctx)

	db := s.GetDB()

	// set up some prometheus stuff
	pWatchCount := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "guildcrius",
		Subsystem: "firehose",
		Name:      "watch_count",
		Help:      "Number of watches, by delivery platform and external service",
	}, []string{"platform", "service"})
	pNotificationsSeen := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "guildcrius",
		Subsystem: "firehose",
		Name:      "notification_count",
		Help:      "Number of notifications provided *to* FH, grouped by external service",
	}, []string{"service"})
	pNotificationsSent := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "guildcrius",
		Subsystem: "firehose",
		Name:      "sent_notifications_count",
		Help:      "Number of notifications sent *by* FH, by delivery platform and external service",
	}, []string{"platform", "service"})

	fh := &firehose{
		db:                 db,
		permissions:        permissions,
		settings:           s,
		glimwsc:            glimwsc.New(os.Getenv("GLIM_CID"), glimwsc.AuthModeClientID),
		ctx:                ctx,
		ytDone:             make(chan bool),
		pWatchCount:        pWatchCount,
		pNotificationsSeen: pNotificationsSeen,
		pNotificationsSent: pNotificationsSent,
	}

	http.HandleFunc("/firehose/sub/twitch", fh.twitchEndpoint)
	http.HandleFunc("/firehose/sub/youtube", fh.youtubeEndpoint)

	err := fh.startConnections()
	if err != nil {
		return nil, err
	}

	return map[string]cc.CommandHandler{
		"addfhwatch": fh.AddFHWatch,
		"remfhwatch": fh.RemoveFHWatch,
	}, nil
}

func (f *firehose) AddFHWatch(m *cc.MessageContext, args []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	isServerAdmin, err := f.permissions.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}

	if !isServerAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	if len(args) < 4 {
		m.Send("Usage: `addfhwatch [service] [username (or channel URL for youtube)] [message] #channel`")
		return nil
	}

	service := firehoseService(args[0])
	if !service.IsValid() {
		m.Send("I currently support Glimesh, Twitch and Youtube")
		return nil
	}

	deliveryChannelID := args[3][2 : len(args[3])-1]
	extID, err := getExtID(service, args[1], f.settings)
	if errors.Is(err, ErrNoYTMatch) {
		m.Send("It seems like you've not specified a URL, quirks with youtube require we use a URL," +
			"please give it in the form https://youtube.com/channel/[channel_id], where the channel ID is a" +
			"long string of letters and numbers")
		return nil
	}
	if err != nil {
		return err
	}

	// let's put it in the db
	_, err = f.db.Exec(`INSERT INTO firehose__notifs (realm_id, channel_id, ext_id, service, message, platform)
		VALUES ($1, $2 ,$3, $4, $5, $6)`, m.RealmID, deliveryChannelID, extID, service, args[2], platform.ToString())
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("I added a notification for channel %s on service %s to go to channel %s", args[1], service, args[3]))

	// do we even *need* to connect?
	var count int
	err = f.db.Get(&count, "SELECT count(*) FROM firehose__notifs WHERE service=$1 AND ext_id=$2", service, extID)
	if err != nil {
		return err
	}

	if count > 1 {
		// we don't need to connect, as we already are and any notifs will get funneled through automatically by
		// virtue of being in the db.
		return nil
	}

	f.pWatchCount.WithLabelValues(platform.ToString(), string(service)).Inc()

	switch service {
	case _FirehoseServiceGlimesh:
		err = f.glimeshBeginSub(*extID)
	case _FirehoseServiceTwitch:
		err = f.twitchBeginSub(*extID)
	case _FirehoseServiceYoutube:
		err = f.ytBeginSub(*extID)
	}

	if err != nil {
		return err
	}

	return nil
}

func (f *firehose) RemoveFHWatch(m *cc.MessageContext, args []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	isServerAdmin, err := f.permissions.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}

	if !isServerAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	if len(args) < 3 {
		m.Send("Usage: `remfhwatch [service] [username (or channel URL for youtube)] #channel`")
		return nil
	}

	service := firehoseService(args[0])
	if !service.IsValid() {
		m.Send("I currently support Glimesh, Twitch and Youtube")
		return nil
	}

	deliveryChannelID := args[2][2 : len(args[2])-1]
	extID, err := getExtID(service, args[1], f.settings)
	if errors.Is(err, ErrNoYTMatch) {
		m.Send("It seems like you've not specified a URL, quirks with youtube require we use a URL," +
			"please give it in the form https://youtube.com/channel/[channel_id], where the channel ID is a" +
			"long string of letters and numbers")
		return nil
	}
	if err != nil {
		return err
	}

	_, err = f.db.Exec(`DELETE FROM firehose__notifs
		WHERE realm_id=$1 AND platform=$2 AND service=$3 AND ext_id=$4 AND channel_id=$5`,
		m.RealmID, platform.ToString(), service, extID, deliveryChannelID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Successfully disabled the notification for channel %s on service %s", args[1], args[0]))

	// should we disconnect?
	var count int
	err = f.db.Get(&count, "SELECT count(*) FROM firehose__notifs WHERE service=$1 AND ext_id=$2", service, extID)
	if err != nil {
		return err
	}

	if count > 0 {
		// we shouldn't disconnect, as we have others servers subbed.
		return nil
	}

	f.pWatchCount.WithLabelValues(platform.ToString(), string(service)).Dec()

	switch service {
	case _FirehoseServiceGlimesh:
		// Disconnecting isn't a thing on Glimesh (or anywhere in graphql, per a conv I had with clone1018 on the glimesh discord)
		break
	case _FirehoseServiceTwitch:
		err = f.twitchEndSub(*extID)
	case _FirehoseServiceYoutube:
		err = f.ytEndSub(*extID)
	}
	if err != nil {
		return err
	}

	return nil
}
