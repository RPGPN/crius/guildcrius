package firehose

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/endpoints"
	"io"
	"net/http"
	"os"
	"time"
)

// Twitch now have their own eventsub system so we have to use that :eyeroll:

const _TwitchHelixAPIRoot = "https://api.twitch.tv/helix"

var twitchCCConfig = clientcredentials.Config{
	ClientID:     os.Getenv("TWITCH_CID"),
	ClientSecret: os.Getenv("TWITCH_SEC"),
	TokenURL:     endpoints.Twitch.TokenURL,
	AuthStyle:    oauth2.AuthStyleInParams,
}

func twitchGetChannelIDFromUsername(username string, s *settings.Settings) (*string, error) {
	cached, hit, err := s.CheckRedisCache(string(_FirehoseServiceTwitch)+".chanid", username)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	client := twitchCCConfig.Client(context.Background())

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/users?login=%s", _TwitchHelixAPIRoot, username), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Client-Id", os.Getenv("TWITCH_CID"))

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var data struct {
		Data []struct {
			ID string `json:"id"`
		}
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	err = s.AddToCache(string(_FirehoseServiceTwitch)+".chanid", username, data.Data[0].ID, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &(data.Data[0].ID), nil
}

type twitchSubInfoTransport struct {
	Method   string  `json:"method"`
	Callback string  `json:"callback"`
	Secret   *string `json:"secret"`
}

type twitchSubInfo struct {
	ID        *string                `json:",omitempty"`
	Type      string                 `json:"type"`
	Version   string                 `json:"version"`
	Condition map[string]interface{} `json:"condition"`
	Transport twitchSubInfoTransport `json:"transport"`
	Status    string
}

func (f *firehose) twitchBeginSub(id string) error {
	secret := os.Getenv("TWITCH_HOOK_SECRET")

	data := twitchSubInfo{
		Type:    "stream.online",
		Version: "1",
		Condition: map[string]interface{}{
			"broadcaster_user_id": id,
		},
		Transport: twitchSubInfoTransport{
			Method:   "webhook",
			Callback: os.Getenv("WEBSUB_URL") + "/firehose/sub/twitch",
			Secret:   &secret,
		},
	}

	encodedData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, _TwitchHelixAPIRoot+"/eventsub/subscriptions", bytes.NewReader(encodedData))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Client-Id", os.Getenv("TWITCH_CID"))

	res, err := twitchCCConfig.Client(context.Background()).Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusAccepted {
		return errors.New(res.Status)
	}

	return nil
}

func (f *firehose) twitchEndSub(userID string) error {
	var subID string
	err := f.db.Get(&subID, "SELECT sub_id FROM firehose_i_subs WHERE to_cond=$1 AND service=$2", userID, _FirehoseServiceTwitch)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodDelete, _TwitchHelixAPIRoot+"/eventsub/subscriptions?id="+subID, nil)
	if err != nil {
		return err
	}

	req.Header.Set("Client-Id", os.Getenv("TWITCH_CID"))

	res, err := twitchCCConfig.Client(context.Background()).Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusNoContent {
		return errors.New(res.Status)
	}

	// remove it from the table
	// remove tracking of this sub
	_, err = f.db.Exec("DELETE FROM firehose_i_subs WHERE sub_id=$1 AND service=$2", subID,
		_FirehoseServiceTwitch)
	if err != nil {
		return err
	}

	return nil
}

type twitchStreamInfo struct {
	GameName    string `json:"game_name"`
	ViewerCount int    `json:"viewer_count"`
	Title       string
}

func twitchGetStreamInfo(userID string) (*twitchStreamInfo, error) {
	req, err := http.NewRequest(http.MethodGet, _TwitchHelixAPIRoot+"/streams?user_id="+userID, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Client-Id", os.Getenv("TWITCH_CID"))

	res, err := twitchCCConfig.Client(context.Background()).Do(req)
	if err != nil {
		return nil, err
	}

	streamInfo := struct {
		Data []*twitchStreamInfo
	}{}

	err = json.NewDecoder(res.Body).Decode(&streamInfo)
	if err != nil {
		return nil, err
	}

	return streamInfo.Data[0], nil
}

func twitchCheckSignature(r *http.Request, body []byte) (bool, error) {
	// what signature do we expect? (it's hex encoded)
	expectedHexString := r.Header.Get("Twitch-Eventsub-Message-Signature")
	if len(expectedHexString) < 7 {
		return false, nil
	}

	expectedBytes, err := hex.DecodeString(expectedHexString[7:])
	if err != nil {
		return false, err
	}

	// get the headers also used in the calc
	messageID := r.Header.Get("Twitch-Eventsub-Message-Id")
	messageTimestamp := r.Header.Get("Twitch-Eventsub-Message-Timestamp")

	hmacMessage := messageID + messageTimestamp + string(body)

	hasher := hmac.New(sha256.New, []byte(os.Getenv("TWITCH_HOOK_SECRET")))
	_, err = hasher.Write([]byte(hmacMessage))
	if err != nil {
		return false, err
	}

	signature := hasher.Sum(nil)

	if !hmac.Equal(signature, expectedBytes) {
		return false, nil
	}

	return true, nil
}

func (f *firehose) twitchEndpoint(w http.ResponseWriter, r *http.Request) {
	// before anything, verify the message
	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Error(err)
		return
	}

	isValid, err := twitchCheckSignature(r, body)
	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Error(err)
		return
	}
	if !isValid {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Warnf("Got invalid signature")
		http.Error(w, "invalid signature", http.StatusForbidden)
		return
	}

	// one of the things twitch suggests to do is to ignore messages over 10m old
	sentAt, err := time.Parse(time.RFC3339Nano, r.Header.Get("Twitch-Eventsub-Message-Timestamp"))
	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Error(err)
		return
	}

	if sentAt.Add(time.Minute * 10).Before(time.Now()) {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Warnf("Event %s too old. Has TS %s",
			r.Header.Get("Twitch-Eventsub-Message-Id"), sentAt.String())
		return
	}

	// twitch tells us what the message type is in the headers
	messageType := r.Header.Get("Twitch-Eventsub-Message-Type")
	switch messageType {
	case "webhook_callback_verification":
		{
			err = f.twitchHandleVerification(w, body)
		}
	case "notification":
		{
			// accept
			w.WriteHeader(http.StatusAccepted)

			f.pNotificationsSeen.WithLabelValues(string(_FirehoseServiceTwitch)).Inc()

			err = f.twitchHandleNotification(body)
		}
	case "revocation":
		{
			// accept
			w.WriteHeader(http.StatusAccepted)
			err = f.twitchHandleRevocation(body)
		}
	}

	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceTwitch).Error(err)
		return
	}
}

func (f *firehose) twitchHandleVerification(w http.ResponseWriter, body []byte) error {
	data := struct {
		Challenge    string
		Subscription twitchSubInfo
	}{}

	err := json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	w.Write([]byte(data.Challenge))

	// now store the sub in the db
	_, err = f.db.Exec("INSERT INTO firehose_i_subs (sub_id, service, to_cond, to_event) VALUES ($1, $2, $3, $4)",
		data.Subscription.ID, _FirehoseServiceTwitch, data.Subscription.Condition["broadcaster_user_id"],
		data.Subscription.Type)
	if err != nil {
		return err
	}

	return nil
}

func (f *firehose) twitchHandleNotification(body []byte) error {
	data := struct {
		Event struct {
			UserID string `json:"broadcaster_user_id"`
			Login  string `json:"broadcaster_user_login"`
			Name   string `json:"broadcaster_user_name"`
			Type   string
		}
		Subscription twitchSubInfo
	}{}

	err := json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	toNotify, err := getServersToNotify(f.db, data.Event.UserID, _FirehoseServiceTwitch)
	if err != nil {
		return err
	}

	streamInfo, err := twitchGetStreamInfo(data.Event.UserID)
	if err != nil {
		return err
	}

	err = f.doNotifyServers(toNotify, &fhGoLive{
		Username: data.Event.Name,
		Service:  _FirehoseServiceTwitch,
		Category: streamInfo.GameName,
		Title:    streamInfo.Title,
		Link:     fmt.Sprintf("https://twitch.tv/%s", data.Event.Name),
	})
	if err != nil {
		return err
	}

	return nil
}

func (f *firehose) twitchHandleRevocation(body []byte) error {
	// we just need to pull it from the list of subs
	// what we should probably do is send a message saying it's been revoked but eh
	data := struct {
		Subscription twitchSubInfo
	}{}

	err := json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	// what is our status?
	// this ignores authorization_revoked because there's not really any way for that to be possible
	switch data.Subscription.Status {
	case "notification_failures_exceeded":
		{
			// remove tracking of this sub
			_, err := f.db.Exec("DELETE FROM firehose_i_subs WHERE sub_id=$1 AND service=$2", data.Subscription.ID,
				_FirehoseServiceTwitch)
			if err != nil {
				return err
			}

			// we should just try to reconnect. this might happen after extended downtime or something
			err = f.twitchBeginSub(data.Subscription.Condition["broadcaster_user_id"].(string))
			if err != nil {
				return err
			}
		}
	case "user_removed":
		{
			// the user isn't on twitch any more or something along those lines. yoink their notifs
			_, err := f.db.Exec("DELETE FROM firehose_i_subs WHERE sub_id=$1 AND service=$2", data.Subscription.ID,
				_FirehoseServiceTwitch)
			if err != nil {
				return err
			}

			// and pull it from the main table
			_, err = f.db.Exec("DELETE FROM firehose__notifs WHERE ext_id=$1 AND service=$2")
			if err != nil {
				return err
			}
		}
	}

	return nil
}
