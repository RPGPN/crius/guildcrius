package firehose

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	"gitlab.com/ponkey364/glimwsc"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const _GlimWSCSubStartQuery = `subscription ($id: ID) {
  channel (id: $id) {
    id
    status
    stream {
      category {
        name
      }
      title
    }
    streamer {
      username
    }
  }
}`

type glimeshStreamNotif struct {
	Channel struct {
		ID     string
		Status string
		Stream *struct {
			Title    string
			Category struct {
				Name string
			}
		}
		Streamer struct {
			Username string
		}
	}
}

func glimGetChannelIDFromUsername(username string, s *settings.Settings) (*string, error) {
	cached, hit, err := s.CheckRedisCache(string(_FirehoseServiceGlimesh)+".chanid", username)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	const query = `query ($username: String) {
  channel(username: $username) {
    id
  }
}`

	bodydata, err := json.Marshal(&struct {
		Query     string            `json:"query"`
		Variables map[string]string `json:"variables"`
	}{
		Query: query,
		Variables: map[string]string{
			"username": username,
		},
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, "https://glimesh.tv/api", bytes.NewReader(bodydata))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Client-ID "+os.Getenv("GLIM_CID"))
	req.Header.Add("content-type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	resp := &struct {
		Data struct {
			Channel struct {
				ID string
			}
		}
	}{}

	err = json.Unmarshal(data, resp)
	if err != nil {
		return nil, err
	}

	err = s.AddToCache(string(_FirehoseServiceGlimesh)+".chanid", username, resp.Data.Channel.ID, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &resp.Data.Channel.ID, nil
}

func (f *firehose) glimeshBeginSub(channel string) error {
	return f.glimwsc.StartSubscription(_GlimWSCSubStartQuery, map[string]interface{}{
		"id": channel,
	}, f.glimeshOnSub)
}

func (f *firehose) glimeshOnSub(_ *glimwsc.Session, data json.RawMessage) {
	stream := &glimeshStreamNotif{}
	err := json.Unmarshal(data, stream)
	if err != nil {
		logrus.Error(err)
		return
	}

	// is the channel offline?
	if stream.Channel.Status == "OFFLINE" || stream.Channel.Stream == nil {
		// there's no point in handling this.
		return
	}

	f.pNotificationsSeen.WithLabelValues(string(_FirehoseServiceGlimesh)).Inc()

	// are there any servers we need to notify?
	toNotify, err := getServersToNotify(f.db, stream.Channel.ID, _FirehoseServiceGlimesh)
	if err != nil {
		logrus.Error(err)
		return
	}

	err = f.doNotifyServers(toNotify, &fhGoLive{
		Username: stream.Channel.Streamer.Username,
		Service:  _FirehoseServiceGlimesh,
		Category: stream.Channel.Stream.Category.Name,
		Title:    stream.Channel.Stream.Title,
		Link:     fmt.Sprintf("https://glimesh.tv/%s", stream.Channel.Streamer.Username),
	})

	if err != nil {
		logrus.Error(err)
		return
	}
}
