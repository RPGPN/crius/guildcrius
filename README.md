# GuildCrius

## Building
This repository contains all of the modifications and plugins used on the hosted version of the bot.

The steps here are the same no matter which option you choose.

1. Install [Go](https://golang.org). This project currently requires go 1.16
2. Set up a postgres database
3. Get a Discord bot token on their [developer website](https://discord.com/developers/applications)
4. `git clone` this repo
5. `go build .`
6. Provide the required [Environment Variables](#environment-variables)
8. Beep boop.

## Environment Variables
|Name|Value|
|---|---|
|PREFIX|What the bot should expect commands to be prefixed with (i.e. $source, where $ is the prefix)|
|DISCORD_TOKEN|The Discord bot token|
|LOG_LVL|Only log entries with this severity or anything above it (see [Logrus Docs](https://pkg.go.dev/github.com/sirupsen/logrus#readme-level-logging))|
|DATABASE_URL|A postgres database URI (i.e. `postgres://[USERNAME]:[PASSWORD]@[HOST]/[DB]`|
|DSC_OWNER_ID|The Discord ID of the bot's owner.|
|WEBSUB_URL|The URL to use for Webhooks / WebSub for Twitch and Youtube notifications. Must be accessible from the internet|
|TWITCH_CID|The Twitch Client ID to use for authorization|
|TWITCH_SEC|The Twitch Client Secret to use for authorization|
|TWITCH_HOOK_SECRET|The secret to send to Twitch for them to sign webhook messages with|