FROM golang:1.16-alpine AS build
RUN apk add --no-cache git

COPY . /app
WORKDIR /app

RUN CGO_ENABLED=0 go install
RUN CGO_ENABLED=0 go build -o app

FROM alpine

RUN apk --update add ca-certificates && \
    rm -rf /var/cache/apk/*

COPY --from=build /app/app /app/app
WORKDIR /app

CMD ["./app"]