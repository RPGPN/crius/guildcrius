package permissions

const CMD_SERVER_OWNER_ONLY = "```diff\n- (Server Owner only)```"
const CMD_SERVER_ADMINS_ONLY = "```diff\n- (Server Admins only)```"
const CMD_SERVER_MODS_ONLY = "```diff\n- (Server Moderators only)```"
const CMD_BOT_OWNER_ONLY = "```diff\n- (Bot Owner only)```"
