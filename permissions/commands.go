package permissions

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/RPGPN/criuscommander/v2"
)

func (p *Permissions) AddServerAdmin(m *criuscommander.MessageContext, args []string) error {
	dmc := m.PlatformData.(*discordgo.MessageCreate)

	if len(args) != 1 {
		m.Send("Usage: giveperm serverAdmin [user] (Server Owner only)")
		return nil
	}

	isOwner, err := p.IsServerOwner(m.Author.ID, dmc.GuildID)
	if err != nil {
		return err
	}
	if !isOwner {
		m.Send(ErrNotSOwner)
		return nil
	}

	db := p.globalSettings.GetDB()

	// See if the user is already known in the server - it might be a promotion!
	perms, err := p.getServerPerms(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		return err
	}

	if perms == nil {
		// we need a new row
		_, err := db.Exec("INSERT INTO server_permissions (server_id, permission, user_id) VALUES ($1,$2,$3)", dmc.GuildID,
			2, dmc.Mentions[0].ID)
		if err != nil {
			return err
		}
		return nil
	}

	if perms.Permission == 2 {
		m.Send("This person is already a server admin")
		return nil
	}

	_, err = db.Exec("UPDATE server_permissions SET permission=2 WHERE server_id=$1 AND user_id=$2", dmc.GuildID, dmc.Mentions[0].ID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Made %s a server admin", dmc.Mentions[0].Username))
	return nil
}

func (p *Permissions) AddServerModerator(m *criuscommander.MessageContext, args []string) error {
	dmc := m.PlatformData.(*discordgo.MessageCreate)

	if len(args) != 1 {
		m.Send("Usage: giveperm serverMod [user] (Server Admins only)")
		return nil
	}

	isAdmin, err := p.IsServerAdmin(m.Author.ID, dmc.GuildID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(ErrNotSAdmin)
		return nil
	}

	db := p.globalSettings.GetDB()

	// See if the user is already known in the server - it might be a promotion!
	perms, err := p.getServerPerms(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		return err
	}

	if perms == nil {
		// we need a new row
		_, err := db.Exec("INSERT INTO server_permissions (server_id, permission, user_id) VALUES ($1,$2,$3)", dmc.GuildID,
			1, dmc.Mentions[0].ID)
		if err != nil {
			return err
		}
		return nil
	}

	if perms.Permission == 2 {
		m.Send("You cannot demote this person")
		return nil
	}

	if perms.Permission == 1 {
		m.Send("This person is already a server moderator")
		return nil
	}

	_, err = db.Exec("UPDATE server_permissions SET permission=1 WHERE server_id=$1 AND user_id=$2", dmc.GuildID,
		dmc.Mentions[0].ID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Made %s a server moderator", dmc.Mentions[0].Username))
	return nil
}

func (p *Permissions) RemoveServerAdmin(m *criuscommander.MessageContext, args []string) error {
	dmc := m.PlatformData.(*discordgo.MessageCreate)

	if len(args) != 1 {
		m.Send("Usage: takeperm serve [user] (Server Owner only)")
		return nil
	}

	isOwner, err := p.IsServerOwner(m.Author.ID, dmc.GuildID)
	if err != nil {
		return err
	}
	if !isOwner {
		m.Send(ErrNotSOwner)
		return nil
	}

	perms, err := p.getServerPerms(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		return err
	}

	if perms.Permission != 2 {
		m.Send("You cannot demote this person")
		return nil
	}

	err = p.removeRole(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		m.Send("Couldn't remove the role for " + dmc.Mentions[0].Username)
		return err
	}

	return nil
}

func (p *Permissions) RemoveServerModerator(m *criuscommander.MessageContext, args []string) error {
	dmc := m.PlatformData.(*discordgo.MessageCreate)

	if len(args) != 1 {
		m.Send("Usage: takeperm serverMod [user] (Server Admins only)")
		return nil
	}

	isAdmin, err := p.IsServerAdmin(m.Author.ID, dmc.GuildID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(ErrNotSAdmin)
		return nil
	}

	perms, err := p.getServerPerms(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		return err
	}

	if perms.Permission != 1 {
		m.Send("You cannot demote this person")
		return nil
	}

	err = p.removeRole(dmc.Mentions[0].ID, dmc.GuildID)
	if err != nil {
		m.Send("Couldn't remove the role for " + dmc.Mentions[0].Username)
		return err
	}

	return nil
}
