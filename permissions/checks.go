package permissions

func (p *Permissions) IsBotOwner(userID string) bool {
	cfgMap := p.ctx.Value("config").(map[string]interface{})
	return cfgMap["DiscordOwner"].(string) == userID
}

func (p *Permissions) IsServerOwner(userID string, serverID string) (bool, error) {
	var serverOwner string

	err := p.globalSettings.GetDB().Get(&serverOwner, "SELECT owner_id FROM servers WHERE server_id=$1", serverID)
	if err != nil {
		return false, err
	}

	if serverOwner == userID {
		return true, nil
	}
	return false, nil
}

func (p *Permissions) IsServerAdmin(userID string, serverID string) (bool, error) {
	isOwner, err := p.IsServerOwner(userID, serverID)
	if err != nil {
		return false, err
	}
	if isOwner {
		return true, nil
	}

	perms, err := p.getServerPerms(userID, serverID)

	if err != nil {
		return false, err
	}
	if perms == nil {
		return false, nil
	}

	if perms.Permission == 2 {
		return true, nil
	}

	return false, nil
}

func (p *Permissions) IsServerMod(userID string, serverID string) (bool, error) {
	isAdmin, err := p.IsServerAdmin(userID, serverID)
	if err != nil {
		return false, err
	}
	if isAdmin {
		return true, nil
	}

	perms, err := p.getServerPerms(userID, serverID)

	if err != nil {
		return false, err
	}
	if perms == nil {
		return false, nil
	}

	if perms.Permission == 1 {
		return true, nil
	}

	return false, nil
}
